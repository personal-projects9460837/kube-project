output "prometheus_dns_name" {
  value = aws_lb.prometheus_load_balancer.dns_name
}

output "prometheus_zone_id" {
  value = aws_lb.prometheus_load_balancer.zone_id
}

