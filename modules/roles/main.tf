#Create IAM policy with a policy document to allow Ansible server perform specific actions on AWS 
#account to discover instances created by autoscaling group without escalating the Ansible server priviledges.

data "aws_iam_policy_document" "ansible-policydoc" {
  statement {
    effect = "Allow"
    actions = [
        "ec2:Describe*",
        "autoscaling:Describe*",
        "ec2:DescribeTags*"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "ansible-policy" {
  name = "ansible-project"
  path = "/"
  policy = data.aws_iam_policy_document.ansible-policydoc.json
}

#Create IAM role with a policy document to allow Ansible server assume role
data "aws_iam_policy_document" "ansible-policydoc-role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ansible-role" {
  name = "ansible-aws-role"
  path = "/"
  assume_role_policy = data.aws_iam_policy_document.ansible-policydoc-role.json
}

#Attach the IAM policy to the IAM role created
resource "aws_iam_role_policy_attachment" "ansible-policy-role-attach" {
  role = aws_iam_role.ansible-role.name
  policy_arn = aws_iam_policy.ansible-policy.arn
}

#Create IAM instance profile to be attached to our Ansible server
resource "aws_iam_instance_profile" "ansible-iam-profile" {
  name = "ansible-project-profile"
  role = aws_iam_role.ansible-role.name
}