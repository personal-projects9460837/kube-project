locals {
  jenkins_user_data = <<-EOF
#!/bin/bash
sudo hostnamectl set-hostname jenkins
sudo apt update
sudo apt install git -y
sudo apt install default-jre -y
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker 
sudo usermod -aG docker ubuntu
sudo chmod 777 /var/run/docker.sock
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
    /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins -y
sudo snap install kubectl --classic
sudo apt install gettext-base
EOF
}