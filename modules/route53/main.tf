# get details about a route 53 hosted zone
data "aws_route53_zone" "hosted_zone" {
  name         = var.domain_name
  private_zone = false
}

# create a record set for jenkins
resource "aws_route53_record" "jenkins_record" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = var.jenkins_record_name
  type    = "A"

  alias {
    name                   = var.jenkins_lb_dns_name
    zone_id                = var.jenkins_lb_zone_id
    evaluate_target_health = true
  }
}

# create a record set for production
resource "aws_route53_record" "prod_record" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = var.prod_record_name
  type    = "A"

  alias {
    name                   = var.prod_lb_dns_name
    zone_id                = var.prod_lb_zone_id
    evaluate_target_health = true
  }
}

# CREATE A RECORD FOR THE STAGE ENVIRONMENT
resource "aws_route53_record" "stage_record" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = var.stage_record_name
  type    = "A"

   alias {
    name                   = var.stage-lb-dns-name
    zone_id                = var.stage-lb-zone-id
    evaluate_target_health = true
  } 
}

# CREATE A RECORD FOR PROMETHEUS SERVICE 
resource "aws_route53_record" "prometheus_record" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id 
  name    = var.prometheus_record_name
  type    = "A"

  alias {
    name                   = var.prometheus-lb-dns-name
    zone_id                = var.prometheus-lb-zone-id
    evaluate_target_health = true
  } 
}

# CREATE A RECORD FOR GRAFANA SERVICE
resource "aws_route53_record" "grafana_record" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = var.grafana_record_name
  type    = "A"

  alias {
    name                   = var.grafana-lb-dns-name
    zone_id                = var.grafana-lb-zone-id
    evaluate_target_health = true
  }
}

# request public certificates from the amazon certificate manager.
resource "aws_acm_certificate" "acm_certificate" {
  domain_name               = var.domain_name
  subject_alternative_names = [var.domain_name2]
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

# create a record set in route 53 for domain validatation
resource "aws_route53_record" "route53_record" {
  for_each = {
    for dvo in aws_acm_certificate.acm_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.hosted_zone.zone_id
}

# validate acm certificates
resource "aws_acm_certificate_validation" "acm_certificate_validation" {
  certificate_arn         = aws_acm_certificate.acm_certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.route53_record : record.fqdn]
}

