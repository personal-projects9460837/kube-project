#!/bin/bash
sudo hostnamectl set-hostname ansible
sudo apt update
sudo apt-add-repository ppa:ansible/ansible
sudo apt update -y
sudo apt install ansible -y
sudo apt install python3-pip -y
sudo apt install python3-boto3 -y

# Copying private key into ansible Server
echo "${priv-key}" >> /home/ubuntu/.ssh/myKey
sudo chown -R ubuntu:ubuntu /home/ubuntu/.ssh/myKey
sudo chmod 400 /home/ubuntu/.ssh/myKey

# Executing playbooks
# sudo su -c "ansible-playbook /home/ubuntu/playbooks/installation.yaml" ubuntu
# sudo su -c "ansible-playbook /home/ubuntu/playbooks/master.yaml" ubuntu
# sudo su -c "ansible-playbook /home/ubuntu/playbooks/worker.yaml" ubuntu
# sudo su -c "ansible-playbook /home/ubuntu/playbooks/jenkins.yaml" ubuntu
# sudo su -c "ansible-playbook /home/ubuntu/playbooks/monitoring.yaml" ubuntu
