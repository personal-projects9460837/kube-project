variable "domain_name" {}
variable "domain_name2" {}
variable "jenkins_record_name" {}
variable "prod_record_name" {}
variable "stage_record_name" {}
variable "prometheus_record_name" {}
variable "grafana_record_name" {}
variable "instance-count" {}

variable "sg-cidr" {}

variable "vpc-cidr" {}
variable "subnet-cidr-pub" {
    type = list(string)
}
variable "pub-subnet" {}
variable "subnet-cidr-priv" {
    type = list(string)
}
variable "priv-subnet" {}
variable "route-cidr" {}